from playwright.sync_api import Page

class ProductPage:
    HEADER: str = '//div[@class="app_logo"]'
    TITLE: str = '//span[@class="title"]'
    HAMBURGER_MENU_BUTTON: str = '#react-burger-menu-btn'
    LOGOUT_BUTTON: str = '#logout_sidebar_link'

    HEADER_TEXT: str = 'Swag Labs'
    TITLE_TEXT: str = 'Products'

    def __init__(self, page: Page):
        self.page = page

    def verify_header_text(self):
        assert self.page.inner_text(self.HEADER) == self.HEADER_TEXT
        assert self.page.inner_text(self.TITLE) == self.TITLE_TEXT

    def open_hamburger_menu(self):
        self.page.click(self.HAMBURGER_MENU_BUTTON)

    def logout(self):
        self.open_hamburger_menu()
        self.page.click(self.LOGOUT_BUTTON)