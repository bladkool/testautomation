from playwright.sync_api import Page

class LoginPage:
    HEADER: str = '//div[@class="login_logo"]'
    LOGIN_BUTTON: str = '#login-button'
    ERROR: str = '//h3[@data-test="error"]'
    USERNAME: str = '#user-name'
    PASSWORD: str = '#password'

    HEADER_TEXT: str = 'Swag Labs'

    def __init__(self, page: Page):
        self.page = page

    def login(self, username, password):
        self.page.fill(self.USERNAME, username)
        self.page.fill(self.PASSWORD, password)
        self.page.click(self.LOGIN_BUTTON)
        

    def verify_header_text(self):
        assert self.page.is_visible(self.HEADER)
        assert self.page.inner_text(self.HEADER) == self.HEADER_TEXT

    def has_error_message(self, message):
        if len(message):
            assert self.page.is_visible(self.ERROR)
            assert self.page.inner_text(self.ERROR) == message
