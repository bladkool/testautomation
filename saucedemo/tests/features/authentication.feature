Feature: User Authentication

  Scenario: Successful login with valid credentials
    Given the user is on the SauceDemo login page
    When the user logs in with valid credentials
    Then the user sees the inventory page

  Scenario: Logout from SauceDemo
    Given the user is logged in to SauceDemo
    When the user logs out
    Then the user should be redirected to the login page

  Scenario: Unsuccessful login with invalid credentials
    Given the user is on the SauceDemo login page
    When the user logs in with invalid credentials
    Then the user sees the error message "Epic sadface: Username and password do not match any user in this service"

   Scenario Outline: Invalid Login
    Given the user is not logged in
    When the user logs in as user "<username>" with password "<password>"
    Then the login fails with "<error>" message

    Examples:
      | username      | password         | error                                                                     |
      | standard_user | secret_sauce     |                                                                           |
      | standard_user |                  | Epic sadface: Password is required                                        |
      |               | secret_sauce     | Epic sadface: Username is required                                        |
      |               |                  | Epic sadface: Username is required                                        |
      | invalid_user  | invalid_password | Epic sadface: Username and password do not match any user in this service |
      | standard_user | invalid_password | Epic sadface: Username and password do not match any user in this service |
      | invalid_user  | secret_sauce     | Epic sadface: Username and password do not match any user in this service |