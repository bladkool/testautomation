import os

from dotenv import load_dotenv
from workflows import workflows
from pytest_bdd import scenarios, given, when, then, parsers


load_dotenv()

BASE_URL = os.environ.get("BASE_URL")
STANDARD_USR = os.environ.get("STANDARD_USR")
PASSWORD = os.environ.get("PASSWORD")

scenarios('../features/authentication.feature')

def test_successful_login_with_valid_credentials():
    print('Successful login with valid credentials')


@given("the user is on the SauceDemo login page")
def go_to_login_page(page):
    workflows.open_login_page(page, BASE_URL)


@when("the user logs in with valid credentials")
def login_with_valid_credentials(page):
    workflows.login(page, BASE_URL, STANDARD_USR, PASSWORD)


@then("the user sees the inventory page")
def open_inventory_page(page):
    workflows.validate_product_page(page)

def test_logout_from_saucedemo():
        print('Logout from SauceDemo')


@given("the user is logged in to SauceDemo")
def logged_in(page):
    workflows.login(page, BASE_URL, STANDARD_USR, PASSWORD)
    workflows.validate_product_page(page)


@when("the user logs out")
def click_the_logout_button(page):
    workflows.logout(page)


@then("the user should be redirected to the login page")
def redirected_to_the_login_page(page):
    workflows.validate_login_page(page)

@when("the user logs in with invalid credentials")
def click_the_logout_button(page):
    workflows.login(page, BASE_URL, "standard_users", PASSWORD)

@then('the user sees the error message "Epic sadface: Username and password do not match any user in this service"')
def error_message(page):
    workflows.validate_error_message(page, "Epic sadface: Username and password do not match any user in this service")

@given("the user is not logged in")
def not_logged_in(page):
    workflows.open_login_page(page, BASE_URL)

@when(parsers.parse('the user logs in as user "{username}" with password "{password}"'))
@when(parsers.re('the user logs in as user "(?P<username>.*?)" with password "(?P<password>.*?)"'))
def invalid_username_password(page, username, password):
    workflows.login(page, BASE_URL, username, password)

@then(parsers.parse('the login fails with "{error}" message'))
@then(parsers.re('the login fails with "(?P<error>.*?)" message'))
def error_message(page, error):
    workflows.validate_error_message(page, error)