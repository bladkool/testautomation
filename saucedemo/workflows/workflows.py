from pages.product_page import ProductPage
from pages.login_page import LoginPage


def open_login_page(page, url):
    login_page = LoginPage(page)
    page.goto(url)
    login_page.verify_header_text()

def validate_login_page(page):
    login_page = LoginPage(page)
    login_page.verify_header_text()

def login(page, url, username, password):
    login_page = LoginPage(page)
    page.goto(url)
    login_page.login(username, password)

def validate_product_page(page):
    product_page = ProductPage(page)
    product_page.verify_header_text()

def logout(page):
    product_page = ProductPage(page)
    product_page.logout()

def validate_error_message(page, error):
    login_page = LoginPage(page)
    login_page.has_error_message(error)